const
    express = require('express'),
    bodyParser = require('body-parser'),
    cors = require('cors'),
    serverless = require("serverless-http");
const
    port = 3001,
    users = [{id: 1, name: "sudarshana"}, {id: 2, name: "lakmal"}];

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

//get all users.
app.get('/user', (req, res) => {
    res.send(users)
});

//get user by id.
app.get('/user/:id', (req, res) => {
    const {id} = req.params;
    res.send(users.find(user => user.id === +id))
});

// app.listen(port, () => console.log(`App listening on port ${port}!`));

const handler = serverless(app);
exports.handler = async (event, context) => {
    return await handler(event, context);
};